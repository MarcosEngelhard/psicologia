using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class DialogManager : MonoBehaviour
{
    private List<string> sentences;
    [SerializeField] private Text dialogueText;
    // Start is called before the first frame update
    private ButtonIndex currentButtonIndex;
    public ButtonIndex CurrentButtonIndex => currentButtonIndex;
    private InteractionPhrases interactionPhrases;
    private GameManager gameManager;
    void Awake()
    {
        sentences = new List<string>();
        interactionPhrases = FindObjectOfType<InteractionPhrases>();
    }
    private void Start()
    {
        gameManager = GameManager.Instance;
    }
    public void StartDialog(ButtonIndex dialogue)
    {
        currentButtonIndex = dialogue;
        StopAnimation();
        if (currentButtonIndex.IsEnd)
        {
            DisplayEndScreen.Instance.DisplayEndScreenCanvasGroup();
            return;
        }
        gameManager.CheckAcceptableInteracoesTerapeuticasButton();
        sentences.Clear(); //clear sentences
        foreach (string sentence in currentButtonIndex.Sentences)
        {
            sentences.Add(sentence);
        }
        
        gameManager.CheckUsedCorrectOrder(currentButtonIndex);
        DisplaySentence();
    }
    public void StopAnimation()
    {
        if (currentButtonIndex.HasAnswer)
        {
            gameManager.StopAnimation(); // Stop animation
        }
    }

    public void DisplaySentence()
    {    
        string sentence = sentences[0];
        dialogueText.text = sentence; // display the sentence       
        gameManager.ClickedButton(currentButtonIndex);
        //Reached the end
        EndDialogue();
    }
    public void DisplayEscutaAtivaSentence() // 1
    {
        dialogueText.text = interactionPhrases.EscutaAtivaPhrase + "\r\n";
        dialogueText.text += interactionPhrases.EncoregamentoPhrase;
        if(sentences.Count >= 2)
        {
            dialogueText.text += sentences[1]; //Display Escuta Ativa sentence if have it
        }
    }
    public void DisplayParafraseSentence() // 2
    {
        dialogueText.text = interactionPhrases.ParafrasePhrase + "\r\n";
        if (sentences.Count >= 3)
        {
            dialogueText.text += sentences[2]; //Display Parafrase sentence if have it
        }
    }
    public void DisplayQuestionamentoSentence() // 3
    {
        dialogueText.text = interactionPhrases.QuestionamentoPhrase + "\r\n";
        if (sentences.Count >= 4)
        {
            dialogueText.text += sentences[3]; //Display Questionamento sentence if have it
        }
    }
    public void DisplayRapportBuildingSentence() //4
    {
        dialogueText.text = interactionPhrases.RapportBuildingPhrase + "\r\n";
        if (sentences.Count >= 5)
        {
            dialogueText.text += sentences[4]; //Display RapportBuilding sentence if have it
        }
    }
    public void DisplayEmpatiaSentence() //5
    {
        dialogueText.text = interactionPhrases.EmpatiaPhrase + "\r\n";
        if (sentences.Count >= 6)
        {
            dialogueText.text += sentences[5]; //Display Empatia sentence if have it
        }
    }
    public void DisplayReflexionSentence() //6
    {
        dialogueText.text = interactionPhrases.ReflexaoPhrase + "\r\n";
        if (sentences.Count >= 7)
        {
            dialogueText.text += sentences[6]; //Display Reflexion sentence if have it
        }
    }
    public void DisplayFocoSentence() //7
    {
        dialogueText.text = interactionPhrases.FocoPhrase + "\r\n";
        if (sentences.Count >= 8)
        {
            dialogueText.text += sentences[7]; //Display Foco sentence if have it
        }
    }
    public void DisplayClarificationSentence() //8
    {
        dialogueText.text = interactionPhrases.ClarificacaoPhrase + "\r\n";
        if (sentences.Count >= 9)
        {
            dialogueText.text += sentences[8]; //Display Clarification if have it
        }
    }
    public void DisplaySummarySentence() //9
    {
        dialogueText.text = interactionPhrases.SumarizacaoPhrase + "\r\n";
        if (sentences.Count >= 10)
        {
            dialogueText.text += sentences[9]; //Display next sentence if have it
        }
    }
    private void EndDialogue()
    {
        if (currentButtonIndex.NextIndex)
        {
            gameManager.UpdateTitle(currentButtonIndex);
            gameManager.SetIndex(currentButtonIndex);
            gameManager.CheckIsOrderRelevant(currentButtonIndex);
        }
        currentButtonIndex.CanShow = false;
        gameManager.DisableAllButtons();
        gameManager.DisplayCorrectButtons();
        StartAnimation();
    }
    private void StartAnimation() // Called by an invoker
    {
        gameManager.TurnOnListenButton(currentButtonIndex);
        gameManager.ScrollBarRectAtTop();
        if (currentButtonIndex.HasAnswer) // if the client answers need to happen the animation
        {
            gameManager.ScrollbarAnimationFunction = gameManager.PlayFadeOutAnimation;
        }
    }
    public void ClearText()
    {
        sentences.Clear();
        dialogueText.text = "";
    }
}
