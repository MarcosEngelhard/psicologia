using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionPhrases : MonoBehaviour
{
    //Escuta
    [SerializeField]
    private string escutaAtivaPhrase = "Para que o cliente sinta que o psic�logo est� a ouvi-lo e est� atento" +
        " � o  psic�logo escuta atentamente, estabelece contacto ocular" +
        " e utiliza interjei��es que refor�am a no��o de que est� a compreender o que o cliente diz ";

    public string EscutaAtivaPhrase => escutaAtivaPhrase;
    //Encorogamento
    [SerializeField]
    private string encoregamentoPhrase = "O psic�logo estabelece contacto ocular, recorre a interjei��es e ao uso de quest�es pertinentes " +
        " e contextualizadas para  garantir que cliente desenvolve ou elabora o seu relato. ";
    public string EncoregamentoPhrase => encoregamentoPhrase;
    //Parafrase
    [SerializeField]
    private string parafrasePhrase = "O psic�logo devolve os pontos-chave do discurso do cliente, para que, em espelho, o cliente possa " +
        "ouvir a repeti��o das suas ideias.";
    public string ParafrasePhrase => parafrasePhrase;
    //Questionamento
    [SerializeField]
    private string questionamentoPhrase = "O psic�logo coloca quest�es que procuram esclarecer, explorar e compreender o cliente. ";
    public string QuestionamentoPhrase => questionamentoPhrase;
    //RapportBuilding
    [SerializeField] 
    private string rapportBuildingPhrase = "O psic�logo expressa disponibilidade emp�tica num ambiente acolhedor, de respeito e de cuidado pelo outro.";
    public string RapportBuildingPhrase => rapportBuildingPhrase;
    //Empatia
    [SerializeField]
    private string empatiaPhrase = "O psic�logo demonstra compreender o cliente de acordo com o seu �quadro de refer�ncia�" +
        " (vis�o de mundo �nica do cliente) ";
    public string EmpatiaPhrase => empatiaPhrase;
    //Reflex�o
    [SerializeField]
    private string reflexaoPhrase = "O psic�logo destaca a emo��o demonstrada pelo cliente ao longo do seu discurso. ";
    public string ReflexaoPhrase => reflexaoPhrase;
    //Foco
    [SerializeField]
    private string focoPhrase = "O psic�logo pede ao cliente que sinalize ou destaque o problema/quest�o mais pertinente.";
    public string FocoPhrase => focoPhrase;
    //Clarifica��o
    [SerializeField]
    private string clarificacaoPhrase = "O psic�logo recorre a(s) pergunta(s) para que possa obter " +
        "uma melhor compreens�o do discurso/hist�ria do cliente.";
    public string ClarificacaoPhrase => clarificacaoPhrase;
    //Sumariza��o
    [SerializeField]
    private string sumarizacaoPhrase = "O psic�logo recorre a uma par�frase longa para destacar/refletir os pontos-chave " +
        "de um tema abordado pelo cliente.";
    public string SumarizacaoPhrase => sumarizacaoPhrase;


}
