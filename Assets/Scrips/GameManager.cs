using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    [SerializeField] private int currentIndex = 0;
    [SerializeField] private List<DialogTrigger> buttonIndexesList = new List<DialogTrigger>();
    public List<DialogTrigger> ButtonIndexesList => buttonIndexesList;
    [SerializeField] private Slider trustSlider;
    public Slider TrustSlider => trustSlider;
    [SerializeField] private ScrollRect scrollRect;
    [SerializeField] private Scrollbar textScrollbar;
    public delegate void ScrollbarAnimation();
    private ScrollbarAnimation scrollbarAnimation;
    public static bool IsScrollbarBeingAnimated { get; set; }
    public ScrollbarAnimation ScrollbarAnimationFunction { get { return scrollbarAnimation; } set { scrollbarAnimation = value; } }
    [System.Serializable]
    private struct InfoEachIndex
    {
        public int currentButtonsResponded;
        public int totalButtons;
    }
    [SerializeField] private InfoEachIndex[] info = new InfoEachIndex[27];
    [SerializeField] private Text subjectTitle;
    [SerializeField] private ListenButtonBehaviour[] listenButtonArray;
    private Animator animator;    
    [SerializeField] private Transform sliderParent;
    //Help see if player follow order
    [SerializeField]private bool hasFollowedOrder = true;
    public bool HasFollowedOrder => hasFollowedOrder;
    [SerializeField] private CurrentFollowingOrder[] currentFollowingOrders = new CurrentFollowingOrder[5];
    [SerializeField]private int currentFollowingIndex = 0;
    [System.Serializable]
    public class CurrentFollowingOrder
    {
        public string name;
        public int currentFollowId = 0;
        public bool isOrderIrrelevant = false;
        public bool hasmistaken = false;
    }
    private DialogManager dialogManager;
    [SerializeField] private int loseErrorOrderPoints = 20;
    [SerializeField] private int earnPointIrrelevantOrder = 2;
    [SerializeField] private int earnFollowingOrder = 4;
    private string initialCode;
    public string InitialCode => initialCode;
    [SerializeField] private int codeLength = 8;
    private const string glyphs = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(Instance.gameObject);
        }
        Instance = this;
        foreach (DialogTrigger dialog in buttonIndexesList)
        {
            if (dialog.Button.HasAnswer)
            {
                info[dialog.Button.Index].totalButtons++;
            }
            dialog.Button.CanShow = true;
        }
        trustSlider.interactable = false; //Player can't move the slider manually
        textScrollbar.direction = Scrollbar.Direction.BottomToTop;
        textScrollbar.value = 1;
        animator = GameObject.FindGameObjectWithTag("HideAnswer").GetComponent<Animator>();
        dialogManager = FindObjectOfType<DialogManager>();
        initialCode = "";
    }
    private void Start()
    {
        ResetTrustAndIndex();
        ResetFollowOrder();
        SetRandomCode();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            QuitGame();
        }
        scrollbarAnimation?.Invoke();
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    private void SetRandomCode()
    {
        //Create a string builder object
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < codeLength; i++)
        {
            var value = glyphs[Random.Range(0, glyphs.Length)];
            builder.Append(value);
        }
        initialCode = builder.ToString();

    }
    public void ResetTrustAndIndex()
    {
        foreach(DialogTrigger dialog in buttonIndexesList)
        {
            if(dialog.CanvasGroup != null)
            {
                dialog.CanvasGroup.interactable = true;
            }
        }
        trustSlider.value = 0;
        currentIndex = 0;
        DisableAllButtons();
        DisplayCorrectButtons();
        dialogManager.ClearText();
        currentFollowingIndex = 0;
    }
    public void ResetFollowOrder()
    {
        hasFollowedOrder = true;
        for (int i = 0; i < currentFollowingOrders.Length; i++)
        {
            currentFollowingOrders[i].currentFollowId = i == 0 ? 1 : 0;
            currentFollowingOrders[i].hasmistaken = false;
        }
    }
    public void ClickedButton(ButtonIndex button) //Increment how many was responded
    {
        if (button.HasAnswer)
        {
            info[button.Index].currentButtonsResponded++;
        }
    }
    public void SetIndex(ButtonIndex button)
    {
        //Change Index
        currentIndex = button.TargetIndex;
    }

    public void DisableAllButtons()
    {
        foreach (DialogTrigger dialog in buttonIndexesList)
        {
            dialog.CanvasGroup.alpha = 0f; // Disable every single button
            dialog.CanvasGroup.blocksRaycasts = false;
        }
    }
    public void IncrementTrustAmountSlider(int amount) //increment slider trust
    {
        trustSlider.value = Mathf.Min(trustSlider.value + amount, trustSlider.maxValue); //Earn trust points
    }
    public void DesincrementTrustAmountSlider(int amount) //remove slider trust
    {
        trustSlider.value = Mathf.Max(trustSlider.value - amount, trustSlider.minValue); //lose trust points
    }
    public void DisplayCorrectButtons()
    {
        foreach (DialogTrigger dialog in buttonIndexesList)
        {
            if (dialog.Button.Index == currentIndex)
            {
                dialog.CanvasGroup.alpha = 1f; //SHow all buttons which has the same index
                dialog.CanvasGroup.blocksRaycasts = true;
            }
        }
    }
    public void StopAnimation()
    {
        scrollbarAnimation = null;
        animator.Play("Idle");
    }
    public void PlayFadeOutAnimation()
    {
        scrollbarAnimation = null;
        StartCoroutine(PlayAnimationCourotine());
    }
    IEnumerator PlayAnimationCourotine()
    {
        yield return new WaitForSeconds(1f);
        animator.Play("FadeOut");
    }
    public void ScrollBarRectAtTop() //Force ScrollbarTop
    {
        Canvas.ForceUpdateCanvases();
        textScrollbar.value = 1;
        scrollRect.normalizedPosition = new Vector2(0, 1); // Scroll to top
        Canvas.ForceUpdateCanvases();
    }
    public void TurnOnListenButton(ButtonIndex buttonIndex)
    {
        if (buttonIndex.GetIsListenButtonActivated)//See if at least one of interactions is aaceptable
        {          
            listenButtonArray[0].TurnListenButtonOn();
        }
        if (buttonIndex.IsParafraseActivated)
        {
            listenButtonArray[1].TurnListenButtonOn();
        }
        if (buttonIndex.IsQuestionamento)
        {
            listenButtonArray[2].TurnListenButtonOn();
        }
        if (buttonIndex.IsRaportBuildingActivated)
        {
            listenButtonArray[3].TurnListenButtonOn();
        }
        if (buttonIndex.IsEmpatiaActivated)
        {
            listenButtonArray[4].TurnListenButtonOn();
        }
        if (buttonIndex.IsReflexionActivated)
        {
            listenButtonArray[5].TurnListenButtonOn();
        }
        if (buttonIndex.IsFocoActivated)
        {
            listenButtonArray[6].TurnListenButtonOn();
        }
        if (buttonIndex.IsClarificationActivated)
        {
            listenButtonArray[7].TurnListenButtonOn();
        }
        if (buttonIndex.IsSummarizedActivated)
        {
            listenButtonArray[8].TurnListenButtonOn();
        }
    }

    public void CheckAcceptableInteracoesTerapeuticasButton()
    {
        foreach (ListenButtonBehaviour buttonBehaviour in listenButtonArray)
        {
            buttonBehaviour.CheckIsActivatedButtonClicked();
        }
    }
    public void CheckIsOrderRelevant(ButtonIndex buttonIndex)
    {
        if (currentFollowingOrders[buttonIndex.TargetIndex].currentFollowId != 0)
            return;
        currentFollowingOrders[buttonIndex.TargetIndex].currentFollowId = buttonIndex.IsIndexRelevant ? 1 : 0;
        currentFollowingIndex = currentFollowingIndex < buttonIndex.TargetIndex ? buttonIndex.TargetIndex : currentFollowingIndex;
    }
    public void CheckUsedCorrectOrder(ButtonIndex buttonIndex) //Check if player is at the correct order
    {
        if (buttonIndex.IsButtonExceptionOrder)
            return;
        if (currentFollowingOrders[buttonIndex.Index].isOrderIrrelevant)
            return;
        
        if (currentFollowingIndex > buttonIndex.Index) // It's already in the button bigger
        {
            if(buttonIndex.Index >= 1 && buttonIndex.Index <= 2)
            {
                DesincrementTrustAmountSlider(loseErrorOrderPoints); //Lose points to nor following the order
                DisplayErrorManager.Instance.GetErrorInformation(DisplayErrorManager.ErrorType.WrongOrder);
                return;
            }
        }

        if (currentFollowingOrders[buttonIndex.Index].hasmistaken)
            return;
        if (currentFollowingOrders[buttonIndex.Index].currentFollowId == 0)
            return;  //The order is not important in this one
        if (currentFollowingOrders[buttonIndex.Index].currentFollowId == buttonIndex.IndexRelevant)
        {
            if (!buttonIndex.IsIndefferentOrder) // if the order is not indefferent, increment on in currentfollow
            {
                currentFollowingOrders[buttonIndex.Index].currentFollowId++;
                IncrementTrustAmountSlider(earnFollowingOrder); //increase a few points to following the correct oder so far
            }
            else
            {
                IncrementTrustAmountSlider(earnPointIrrelevantOrder);
            }
            DisplayErrorManager.Instance.GetErrorInformation(DisplayErrorManager.ErrorType.CorrectOrder);
            return;
        }
        DesincrementTrustAmountSlider(loseErrorOrderPoints); //Lose points to nor following the order
        DisplayErrorManager.Instance.GetErrorInformation(DisplayErrorManager.ErrorType.WrongOrder);
        hasFollowedOrder = false;
        currentFollowingOrders[buttonIndex.Index].hasmistaken = true;
    }
    public void UpdateTitle(ButtonIndex buttonIndex)
    {
        subjectTitle.text = buttonIndex.ButtonName;
    }
}
