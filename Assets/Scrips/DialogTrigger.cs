using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DialogTrigger : MonoBehaviour
{
    [SerializeField] private ButtonIndex buttonIndex;
    public ButtonIndex Button => buttonIndex;
    private Text buttonText;
    private DialogManager dialogManager;
    [SerializeField] private bool toggleInteractive = true;
    private CanvasGroup canvasGroup;
    public CanvasGroup CanvasGroup => canvasGroup;
    
    private void Awake()
    {
        buttonText = GetComponentInChildren<Text>();
        canvasGroup = GetComponent<CanvasGroup>();
        buttonText.text = buttonIndex.ButtonName;
        dialogManager = FindObjectOfType<DialogManager>();
    }
    public void TriggerDialogue()
    {
        if (GameManager.IsScrollbarBeingAnimated) // if scroll bar is being animated automatically, don't skip until the animation is complete
        {
            return;
        }
        if (toggleInteractive)
        {
            canvasGroup.interactable = false;
        }
        dialogManager.StartDialog(buttonIndex); // start the next dialog
    }
}
