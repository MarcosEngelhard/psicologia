using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartButton : MonoBehaviour
{
    private CanvasGroup startCanvasgroup;
    // Start is called before the first frame update
    void Start()
    {
        startCanvasgroup = GetComponent<CanvasGroup>();
    }

    public void StartClicked()
    {
        if( startCanvasgroup != null)
        {
            startCanvasgroup.alpha = 0f;
            startCanvasgroup.blocksRaycasts = false;
            startCanvasgroup.interactable = false;
        }
    }
}
