using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayEndScreen : MonoBehaviour
{
    public static DisplayEndScreen Instance;
    [SerializeField] private CanvasGroup endScreen;
    public CanvasGroup Endscreen => endScreen;
    [SerializeField] private Text[] resultsTexts = new Text[9];
    private List<Slider> resultSliders = new List<Slider>();
    [SerializeField] private Transform sliderParent;
    [SerializeField] private Text trustFeedbackText;
    public Text FeedbackText => trustFeedbackText;
    //Below will cound total buttons, used correctly and wrong
    [System.Serializable]
    public struct Info
    {
        public string name;
        public int TotalButtons;
        public int UsedCorrectly;
        public int UsedButtons;
    }
    // 0 - Escuta Ativa; 1 - Parafrase; 2 - Questionamento; 3 - Rapport building; 4 - Empatia; 5 - Reflex�o;
    // 6 - Foco; 7 - Clarifica��o 8- SUmariza��o
    [SerializeField]private Info[] infoArray = new Info[9];
    public Info[] GetInfoArray => infoArray;
    public int EscutaAtivaUsedCorrectly { get { return infoArray[0].UsedCorrectly; } set { infoArray[0].UsedCorrectly = value; } }
    public int EscutaAtivaUsedButtons { get { return infoArray[0].UsedButtons; } set { infoArray[0].UsedButtons = value; } }
    public int ParafraseLegalUsedCorrectly { get { return infoArray[1].UsedCorrectly; } set { infoArray[1].UsedCorrectly = value; } }
    public int ParafraseUsedButtons { get { return infoArray[1].UsedButtons; } set { infoArray[1].UsedButtons = value; } }
    public int QuestionamentoUsedCorrectly { get { return infoArray[2].UsedCorrectly; } set { infoArray[2].UsedCorrectly = value; } }
    public int QuestionamentoUsed { get { return infoArray[2].UsedButtons; } set { infoArray[2].UsedButtons = value; } }
    public int RapportBuildingUsedCorrectly { get { return infoArray[3].UsedCorrectly; } set { infoArray[3].UsedCorrectly = value; } }
    public int RapportBuildingUsed { get { return infoArray[3].UsedButtons; } set { infoArray[3].UsedButtons = value; } }
    public int EmpatiaUsedCorrectly { get { return infoArray[4].UsedCorrectly; } set { infoArray[4].UsedCorrectly = value; } }
    public int EmpatiaUsed { get { return infoArray[4].UsedButtons; } set { infoArray[4].UsedButtons = value; } }
    public int ReflexaoUsedCorrectly { get { return infoArray[5].UsedCorrectly; } set { infoArray[5].UsedCorrectly = value; } }
    public int ReflexaoUsed { get { return infoArray[5].UsedButtons; } set { infoArray[5].UsedButtons = value; } }
    public int FocoUsedCorrectly { get { return infoArray[6].UsedCorrectly; } set { infoArray[6].UsedCorrectly = value; } }
    public int FocoUsed { get { return infoArray[6].UsedButtons; } set { infoArray[6].UsedButtons = value; } }
    public int ClarificacaoUsedCorrectly { get { return infoArray[7].UsedCorrectly; } set { infoArray[7].UsedCorrectly = value; } }
    public int ClarificacaoUsed { get { return infoArray[7].UsedButtons; } set { infoArray[7].UsedButtons = value; } }
    public int SumarizacaoUsedCorrectly { get { return infoArray[8].UsedCorrectly; } set { infoArray[8].UsedCorrectly = value; } }
    public int SumarizacaoUsed { get { return infoArray[8].UsedButtons; } set { infoArray[8].UsedButtons = value; } }
    //End Result for the feedbackTest
    [SerializeField] private ButtonIndex negativeCompetency;
    [SerializeField] private ButtonIndex positiveCompetency;
    [SerializeField] private ButtonIndex negativeOrder;
    [SerializeField] private ButtonIndex positiveOrder;
    private GameManager gameManager;
    [SerializeField] private int minTrustToMedium = 80;

    private void Awake()
    {
        if(Instance != null && Instance != this)
        {
            Destroy(Instance);
        }
        Instance = this;
        endScreen.alpha = 0f;
        endScreen.interactable = false;
        endScreen.blocksRaycasts = false;
        foreach (Transform sliderTransform in sliderParent.transform)
        {
            resultSliders.Add(sliderTransform.GetComponent<Slider>());
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManager.Instance; // Put on start instead of Awake to Avoid assign a null reference
        SetInfoNameCorrect();
        CountTotalIdeal();
    }
    private void SetInfoNameCorrect()
    {
        for (int i = 0; i < infoArray.Length; i++)
        {
            switch (i)
            {
                case 0: infoArray[i].name = "Escuta Ativa e encorajamento"; break;

                case 1:
                    infoArray[i].name = "Parafrase";
                    break;
                case 2:
                    infoArray[i].name = "Questionamento aberto de exploracao";
                    break;
                case 3:
                    infoArray[i].name = "Rapport Building";
                    break;
                case 4:
                    infoArray[i].name = "Empatia";
                    break;
                case 5:
                    infoArray[i].name = "Reflexao";
                    break;
                case 6:
                    infoArray[i].name = "Foco";
                    break;
                case 7:
                    infoArray[i].name = "Clarificacao";
                    break;
                case 8:
                    infoArray[i].name = "Sumarizacao";
                    break;
            }
        }
    }
    private void CountTotalIdeal()
    {
        //See All the correct ideal intera��es terapeuticas
        foreach (DialogTrigger dialog in gameManager.ButtonIndexesList)
        {
            if (dialog.Button.GetIsListenButtonActivated)
            {
                infoArray[0].TotalButtons++;
            }
            if (dialog.Button.IsParafraseActivated)
            {
                infoArray[1].TotalButtons++;
            }
            if (dialog.Button.IsQuestionamento)
            {
                infoArray[2].TotalButtons++;
            }
            if (dialog.Button.IsRaportBuildingActivated)
            {
                infoArray[3].TotalButtons++;
            }
            if (dialog.Button.IsEmpatiaActivated)
            {
                infoArray[4].TotalButtons++;
            }
            if (dialog.Button.IsReflexionActivated)
            {
                infoArray[5].TotalButtons++;
            }
            if (dialog.Button.IsFocoActivated)
            {
                infoArray[6].TotalButtons++;
            }
            if (dialog.Button.IsClarificationActivated)
            {
                infoArray[7].TotalButtons++;
            }
            if (dialog.Button.IsSummarizedActivated)
            {
                infoArray[8].TotalButtons++;
            }
        }
    }
    public void DisplayEndScreenCanvasGroup() //End screen
    {
        endScreen.alpha = 1f;
        endScreen.interactable = true;
        endScreen.blocksRaycasts = true;
        trustFeedbackText.text = gameManager.TrustSlider.value < minTrustToMedium ? negativeCompetency.Sentences[0] : positiveCompetency.Sentences[0];
        trustFeedbackText.text += !gameManager.HasFollowedOrder ? negativeOrder.Sentences[0] : positiveOrder.Sentences[0];
        for (int i = 0; i < resultsTexts.Length; i++)
        {
            resultsTexts[i].text = "- usada: " + infoArray[i].UsedButtons.ToString()
               + " em " + infoArray[i].TotalButtons.ToString() + " ideal \r\n- " + infoArray[i].UsedCorrectly.ToString() + " usado corretamente \r\n- "
               + (infoArray[i].UsedButtons - infoArray[i].UsedCorrectly).ToString() + " usado incorretamente";
            UpdateEachSlider(infoArray[i].UsedButtons, infoArray[i].TotalButtons, infoArray[i].UsedCorrectly, i);
        }
        resultSliders[resultSliders.Count - 1].value = gameManager.TrustSlider.value;
    }
    private void UpdateEachSlider(int UsedButtons, int TotalButtons, int CorrectlyButtons, int _i) //When ending level
    {
        //Each quest�o terapeutica has 3 sliders
        float auxPercentage;
        int firstSliderInt = _i * 3;
        int secondSliderInt = firstSliderInt + 1;
        int thirdSliderInt = secondSliderInt + 1;
        auxPercentage = TotalButtons != 0 ? (float)(UsedButtons / (float)TotalButtons) : 0;
        resultSliders[firstSliderInt].value = Mathf.Min(auxPercentage, 1);
        auxPercentage = UsedButtons != 0 ? (float)(CorrectlyButtons / (float)UsedButtons) : 0;
        resultSliders[secondSliderInt].value = Mathf.Min(auxPercentage, 1);
        auxPercentage = UsedButtons != 0 ? (float)((UsedButtons - CorrectlyButtons) / (float)UsedButtons) : 0;
        resultSliders[thirdSliderInt].value = Mathf.Min(auxPercentage, 1);
    }
}
