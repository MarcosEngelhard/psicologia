using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Button Index Answer")]
public class ButtonIndex : ScriptableObject //It's a scriptable Object
{
    [SerializeField] private int index;
    public int Index => index;
    [SerializeField] private string buttonName;
    public string ButtonName => buttonName;

    [SerializeField] private bool nextchapter = false;
    public bool NextChapter => nextchapter; // if true => get global state

    [TextArea(3, 40)]
    [SerializeField] private string[] sentences;
    public string[] Sentences => sentences;

    private bool canShow = true;
    public bool CanShow { get { return canShow; } set { canShow = value; } }

    [SerializeField] private bool nextIndex = false;
    //Change index
    public bool NextIndex => nextIndex;

    [SerializeField] private int targetIndex = 1;

    public int TargetIndex => targetIndex;
    //Index order relate
    [SerializeField] private bool isTargetIndexRelevant;
    public bool IsIndexRelevant => isTargetIndexRelevant;
    [SerializeField] private bool isButtonExceptiontoOrderRelevant = false;
    public bool IsButtonExceptionOrder => isButtonExceptiontoOrderRelevant;
    [SerializeField] private int currentIndexRelevant = 0;
    public int IndexRelevant => currentIndexRelevant;
    [SerializeField]private bool isIndefferentOrder = false; //A ordem � indeferente
    public bool IsIndefferentOrder => isIndefferentOrder;
    //Acceptable or not competencias terapeuticas
    [SerializeField] private bool isListenButtonActivated; //Escuta ativa
    public bool GetIsListenButtonActivated => (isListenButtonActivated);

    [SerializeField] private bool isParafraseActivated; // parafrase
    public bool IsParafraseActivated => isParafraseActivated;

    [SerializeField] private bool isQuestionamenteExplorationActivated; //questionamento
    public bool IsQuestionamento => isQuestionamenteExplorationActivated;

    [SerializeField] private bool isRapportBuildingActivated; //rapport building
    public bool IsRaportBuildingActivated => isRapportBuildingActivated;

    [SerializeField] private bool isEmpatiaActivated; //empatia
    public bool IsEmpatiaActivated => isEmpatiaActivated;

    [SerializeField] private bool isReflexionActivated; //Reflexao
    public bool IsReflexionActivated => isReflexionActivated;

    [SerializeField] private bool isFocoActivated; //Foco
    public bool IsFocoActivated => isFocoActivated;

    [SerializeField] private bool isClarificationActivated; //Clarification
    public bool IsClarificationActivated => isClarificationActivated;

    [SerializeField] private bool isSummarizedActivated; //Summarized
    public bool IsSummarizedActivated => isSummarizedActivated;
    [SerializeField] private bool hasAnswer = true;
    public bool HasAnswer => hasAnswer;

    [SerializeField] private bool isEnd = false;
    public bool IsEnd => isEnd;
}
