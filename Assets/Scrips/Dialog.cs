using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialog
{
    [SerializeField] private string name;
    [TextArea(3,40)]
    [SerializeField] private string[] sentences;
    public string[] Sentences => sentences;

}
