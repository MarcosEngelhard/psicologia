using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayErrorManager : MonoBehaviour //Display FeedBackManager
{
    public static DisplayErrorManager Instance;
    [SerializeField] private CanvasGroup mainCanvasGroup;
    [SerializeField] private CanvasGroup errorCanvasGroup;
    [SerializeField] private Text positiveFeedbackText;
    [SerializeField] private Text negativeFeedbackText;
    private Color textColor;
    [SerializeField] private float waitToContinueTime = 3f;
    public enum ErrorType
    {
        WrongOrder,CorrectOrder, WrongInfomationTerapeutica, CorrectInformationTerapeutica
    }
    private void Awake()
    {
        errorCanvasGroup.alpha = 0;
        errorCanvasGroup.blocksRaycasts = false;
        if(Instance != null && Instance != this)
        {
            Destroy(Instance.gameObject);
        }
        Instance = this;
    }
    private void Start() // Set both texts transparent
    {
        SetTextTransparent(negativeFeedbackText);
        SetTextTransparent(positiveFeedbackText);
    }
    public void GetErrorInformation(ErrorType _errorType)
    {
        //Get the information to be displayed at the courotine
        switch (_errorType)
        {
            case ErrorType.WrongOrder:
                negativeFeedbackText.text = "Quest�o n�o pertinente nesta fase";
                SetTextOpaque(negativeFeedbackText);
                break;
            case ErrorType.CorrectOrder:
                positiveFeedbackText.text = "Quest�o pertinente nesta fase";
                SetTextOpaque(positiveFeedbackText);
                break;
            case ErrorType.WrongInfomationTerapeutica:
                negativeFeedbackText.text = "Intera��o n�o pertinente nesta Fase";
                SetTextOpaque(negativeFeedbackText);
                break;
            case ErrorType.CorrectInformationTerapeutica:
                positiveFeedbackText.text = "Intera��o pertinente nesta Fase";
                SetTextOpaque(positiveFeedbackText);
                break;
        }
        StartCoroutine(DisplayFeedbackCanvasGroup());
    }
    IEnumerator DisplayFeedbackCanvasGroup()
    {
        errorCanvasGroup.alpha = 1;
        errorCanvasGroup.blocksRaycasts = true;
        mainCanvasGroup.interactable = false;
        yield return new WaitForSeconds(waitToContinueTime); // wait until you can keep doing
        SetTextTransparent(negativeFeedbackText);
        SetTextTransparent(positiveFeedbackText);
        errorCanvasGroup.alpha = 0;
        errorCanvasGroup.blocksRaycasts = false;
        mainCanvasGroup.interactable = true;
    }
    private void SetTextTransparent(Text _text) //Make text transparent
    {
        textColor = _text.color;
        textColor.a = 0f;
        _text.color = textColor;
    }
    private void SetTextOpaque(Text _text) //Make text opaque
    {
        textColor = _text.color;
        textColor.a = 1f;
        _text.color = textColor;
    }
}
