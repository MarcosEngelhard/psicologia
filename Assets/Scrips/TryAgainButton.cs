using UnityEngine;

public class TryAgainButton : MonoBehaviour
{
    private GameManager gameManager;
    [SerializeField]private CSVWriter excelWrite;
    private void Start()
    {
        gameManager = GameManager.Instance;
    }
    public void TryAgainClicked()// Activated when button is clicked
    {
        if(DisplayEndScreen.Instance.Endscreen != null)
        {
            DisplayEndScreen.Instance.Endscreen.alpha = 0f;
            DisplayEndScreen.Instance.Endscreen.interactable = false;
            DisplayEndScreen.Instance.Endscreen.blocksRaycasts = false;
        }
        excelWrite.SetExporButtonInteractable(true);
        gameManager.ResetTrustAndIndex();
        gameManager.ResetFollowOrder();
    }
}
