using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListenButtonBehaviour : MonoBehaviour
{
    private bool isAcceptable;
    [SerializeField] private int incrementSlider = 5;

    private Button button;
    private DialogManager dialogManager;
    private DisplayEndScreen DisplayEndScreen;
    private enum ListenButtonType
    {
        EscutaAtiva, Parafrase, Questionamento, RapperBuilding, Empatio, Reflexion, Foco, Clarification, Summary
    }
    [SerializeField] private ListenButtonType buttonType;
    private delegate void DisplayExactSentence();
    private DisplayExactSentence exactSentence;
    private void Awake()
    {
        button = GetComponent<Button>();
        dialogManager = FindObjectOfType<DialogManager>();
    }
    private void Start()
    {
        DisplayEndScreen = FindObjectOfType<DisplayEndScreen>();
        switch (buttonType)
        {
            case ListenButtonType.EscutaAtiva:
                exactSentence = EscutaAtivaSentence;
                break;
            case ListenButtonType.Parafrase:
                exactSentence = ParafraseSentence;
                break;
            case ListenButtonType.Questionamento:
                exactSentence = QuestionamentoAbertoSentence;
                break;
            case ListenButtonType.RapperBuilding:
                exactSentence = RapporBuildingSentence;
                break;
            case ListenButtonType.Empatio:
                exactSentence = EmpatiaSentence;
                break;
            case ListenButtonType.Reflexion:
                exactSentence = ReflexionSentence;
                break;
            case ListenButtonType.Foco:
                exactSentence = FocoSentence;
                break;
            case ListenButtonType.Clarification:
                exactSentence = ClarificationSentence;
                break;
            case ListenButtonType.Summary:
                exactSentence = SummarySentence;
                break;
        }
    }
    public bool GetIsActivated()
    {
        return isAcceptable;
    }
    public void ListenButtonClicked() //Called by the button clicked
    {
        if (isAcceptable)
        {
            GameManager.Instance.IncrementTrustAmountSlider(incrementSlider);
            DisplayErrorManager.Instance.GetErrorInformation(DisplayErrorManager.ErrorType.CorrectInformationTerapeutica);
        }
        else
        {
            GameManager.Instance.DesincrementTrustAmountSlider(incrementSlider);
            DisplayErrorManager.Instance.GetErrorInformation(DisplayErrorManager.ErrorType.WrongInfomationTerapeutica);
        }
        exactSentence?.Invoke();
        isAcceptable = false;
        button.interactable = false;
        GameManager.Instance.ScrollBarRectAtTop(); //Force Scrollbar at top!
    }
    public void TurnListenButtonOn()
    {
        isAcceptable = true;
        button.interactable = true;
    }
    public void CheckIsActivatedButtonClicked() //Game is not accepted anymore
    {
        isAcceptable = false;
        button.interactable = true;
    }
    private void EscutaAtivaSentence() // = 1
    {
        DisplayEndScreen.EscutaAtivaUsedButtons++;
        dialogManager.DisplayEscutaAtivaSentence();
        if (dialogManager.CurrentButtonIndex.GetIsListenButtonActivated)
        {
            DisplayEndScreen.EscutaAtivaUsedCorrectly++;
        }
    }
    private void ParafraseSentence() // = 2
    {
        DisplayEndScreen.ParafraseUsedButtons++;
        dialogManager.DisplayParafraseSentence();
        if(dialogManager.CurrentButtonIndex.IsParafraseActivated)
        {
            DisplayEndScreen.ParafraseLegalUsedCorrectly++;
        }
    }
    private void QuestionamentoAbertoSentence() // = 3
    {
        DisplayEndScreen.QuestionamentoUsed++;
        dialogManager.DisplayQuestionamentoSentence();
        if (dialogManager.CurrentButtonIndex.IsQuestionamento)
        {
            DisplayEndScreen.QuestionamentoUsedCorrectly++;
        }
    }
    private void RapporBuildingSentence() // =4
    {
        DisplayEndScreen.RapportBuildingUsed++;
        dialogManager.DisplayRapportBuildingSentence();
        if(dialogManager.CurrentButtonIndex.IsRaportBuildingActivated)
        {
            DisplayEndScreen.RapportBuildingUsedCorrectly++;
        }
    }
    private void EmpatiaSentence() // = 5
    {
        DisplayEndScreen.EmpatiaUsed++;
        dialogManager.DisplayEmpatiaSentence();
        if (dialogManager.CurrentButtonIndex.IsEmpatiaActivated)
        {
            DisplayEndScreen.EmpatiaUsedCorrectly++;
        }
    }
    private void ReflexionSentence() // = 6
    {
        DisplayEndScreen.ReflexaoUsed++;
        dialogManager.DisplayReflexionSentence();
        if(dialogManager.CurrentButtonIndex.IsReflexionActivated)
        {
            DisplayEndScreen.ReflexaoUsedCorrectly++;
        }
    }
    private void FocoSentence() // = 7
    {
        DisplayEndScreen.FocoUsed++;
        dialogManager.DisplayFocoSentence();
        if (dialogManager.CurrentButtonIndex.IsFocoActivated)
        {
            DisplayEndScreen.FocoUsedCorrectly++;
        }
    }
    private void ClarificationSentence() // = 8
    {
        DisplayEndScreen.ClarificacaoUsed++;
        dialogManager.DisplayClarificationSentence();
        if(dialogManager.CurrentButtonIndex.IsClarificationActivated)
        {
            DisplayEndScreen.ClarificacaoUsedCorrectly++;
        }
    }
    private void SummarySentence() // = 9
    {
        DisplayEndScreen.SumarizacaoUsed++;
        dialogManager.DisplaySummarySentence();
        if (dialogManager.CurrentButtonIndex.IsSummarizedActivated)
        {
            DisplayEndScreen.SumarizacaoUsedCorrectly++;
        }
    }
}
