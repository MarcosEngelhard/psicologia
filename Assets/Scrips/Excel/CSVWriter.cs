using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class CSVWriter : MonoBehaviour
{
    [SerializeField]private Button ExportButton;
    private string googleBase = "https://docs.google.com/forms/u/0/d/e/1FAIpQLSfoS4V-JtENLQWLxpWCpmLPAl9Wv5pshoRkI_nHLSePxP_8rA/formResponse";

    //Final panel related
    [SerializeField]private CanvasGroup finalPanelCanvasgroup;
    [SerializeField]private Text thanksText;
    private void Awake()
    {
        finalPanelCanvasgroup.alpha = 0f;
        finalPanelCanvasgroup.blocksRaycasts = false;
        finalPanelCanvasgroup.interactable = false;
    }
    public void GetData() //By contunue/export to excel button
    {
        StartCoroutine(PrepareSend());
        SetExporButtonInteractable(false);
        DisplayFinalPanel();
    }
    private IEnumerator PrepareSend()
    {
        WWWForm form = new WWWForm();
        //Feedback text
        form.AddField("entry.1310325111", DisplayEndScreen.Instance.FeedbackText.text);
        //Escuta Ativa
        form.AddField("entry.190358126", DisplayEndScreen.Instance.GetInfoArray[0].UsedButtons);
        form.AddField("entry.765055439", DisplayEndScreen.Instance.GetInfoArray[0].UsedCorrectly);
        form.AddField("entry.1074820320", (DisplayEndScreen.Instance.GetInfoArray[0].UsedButtons - DisplayEndScreen.Instance.GetInfoArray[0].UsedCorrectly));
        //Parafrase
        form.AddField("entry.1686392862", DisplayEndScreen.Instance.GetInfoArray[1].UsedButtons);
        form.AddField("entry.42168741", DisplayEndScreen.Instance.GetInfoArray[1].UsedCorrectly);
        form.AddField("entry.1731221683", (DisplayEndScreen.Instance.GetInfoArray[1].UsedButtons - DisplayEndScreen.Instance.GetInfoArray[1].UsedCorrectly));
        //Questionamento
        form.AddField("entry.1376703055", DisplayEndScreen.Instance.GetInfoArray[2].UsedButtons);
        form.AddField("entry.1067835755", DisplayEndScreen.Instance.GetInfoArray[2].UsedCorrectly);
        form.AddField("entry.1606778940", (DisplayEndScreen.Instance.GetInfoArray[2].UsedButtons - DisplayEndScreen.Instance.GetInfoArray[2].UsedCorrectly));
        //Rapport building
        form.AddField("entry.1466098884", DisplayEndScreen.Instance.GetInfoArray[3].UsedButtons);
        form.AddField("entry.288531903", DisplayEndScreen.Instance.GetInfoArray[3].UsedCorrectly);
        form.AddField("entry.1554943239", (DisplayEndScreen.Instance.GetInfoArray[3].UsedButtons - DisplayEndScreen.Instance.GetInfoArray[3].UsedCorrectly));
        //Empatia
        form.AddField("entry.1030866744", DisplayEndScreen.Instance.GetInfoArray[4].UsedButtons);
        form.AddField("entry.1258495186", DisplayEndScreen.Instance.GetInfoArray[4].UsedCorrectly);
        form.AddField("entry.809217622", (DisplayEndScreen.Instance.GetInfoArray[4].UsedButtons - DisplayEndScreen.Instance.GetInfoArray[4].UsedCorrectly));
        //Reflex�o
        form.AddField("entry.929551332", DisplayEndScreen.Instance.GetInfoArray[5].UsedButtons);
        form.AddField("entry.1146832067", DisplayEndScreen.Instance.GetInfoArray[5].UsedCorrectly);
        form.AddField("entry.1145624252", (DisplayEndScreen.Instance.GetInfoArray[5].UsedButtons - DisplayEndScreen.Instance.GetInfoArray[5].UsedCorrectly));
        //Foco
        form.AddField("entry.822407637", DisplayEndScreen.Instance.GetInfoArray[6].UsedButtons);
        form.AddField("entry.1398276912", DisplayEndScreen.Instance.GetInfoArray[6].UsedCorrectly);
        form.AddField("entry.2098196635", (DisplayEndScreen.Instance.GetInfoArray[6].UsedButtons - DisplayEndScreen.Instance.GetInfoArray[6].UsedCorrectly));
        //Clarifica��o
        form.AddField("entry.277951316", DisplayEndScreen.Instance.GetInfoArray[7].UsedButtons);
        form.AddField("entry.1447295150", DisplayEndScreen.Instance.GetInfoArray[7].UsedCorrectly);
        form.AddField("entry.1177507377", (DisplayEndScreen.Instance.GetInfoArray[7].UsedButtons - DisplayEndScreen.Instance.GetInfoArray[7].UsedCorrectly));
        //Sumariza��o
        form.AddField("entry.847420933", DisplayEndScreen.Instance.GetInfoArray[8].UsedButtons);
        form.AddField("entry.1871992916", DisplayEndScreen.Instance.GetInfoArray[8].UsedCorrectly);
        form.AddField("entry.1117038642", (DisplayEndScreen.Instance.GetInfoArray[8].UsedButtons - DisplayEndScreen.Instance.GetInfoArray[8].UsedCorrectly));
        //Code
        form.AddField("entry.1060007900", GameManager.Instance.InitialCode);

        UnityWebRequest www = UnityWebRequest.Post(googleBase, form );
        yield return www.SendWebRequest();
    }

    public void SetExporButtonInteractable(bool state)
    {
        ExportButton.interactable = state;
    }
    private void DisplayFinalPanel()
    {
        finalPanelCanvasgroup.alpha = 1f;
        finalPanelCanvasgroup.blocksRaycasts = true;
        finalPanelCanvasgroup.interactable = true;
        thanksText.text = "De seguida, convidamo-lo(a) a preencher um breve question�rio em papel. \r\n Escreva no topo do question�rio o seguinte c�digo: " + GameManager.Instance.InitialCode;
    }
}
